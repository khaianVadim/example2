package com.example.example2.controller;

import com.example.example2.domain.Entity;
import com.example.example2.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/entities")
public class EntityController {

    private final EntityService service;

    public EntityController(@Autowired EntityService service) {
        this.service = service;
    }

    @GetMapping("/all-entityes")
    public ResponseEntity<List<Entity>> getAll() {
        return new ResponseEntity(service.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Entity> findById(@PathVariable("id") Long id) {
        return new ResponseEntity(service.findById(id), HttpStatus.OK);
    }

    @PostMapping("/create-update")
    public ResponseEntity<Entity> createUpdate(@RequestBody Entity entity) {
        return new ResponseEntity(service.updateCreate(entity), HttpStatus.CREATED);
    }
}
