package com.example.example2.service.impl;

import com.example.example2.domain.Entity;
import com.example.example2.exception.UserNotFoundException;
import com.example.example2.repository.EntityRepository;
import com.example.example2.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EntityServiceImpl implements EntityService {

    private final EntityRepository repository;

    public EntityServiceImpl(@Autowired EntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Entity> getAll() {
        return repository.findAll();
    }

    @Override
    public Entity findById(Long id) {
        Optional<Entity> entity = repository.findById(id);
        return entity.orElseThrow(() ->
                new UserNotFoundException("entity with id " + id + " not found", HttpStatus.BAD_REQUEST));
    }

    @Override
    public Entity updateCreate(Entity entity) {
        if (entity.getId() == null) {
            return repository.save(entity);
        }
        Optional<Entity> optionalEntity = repository.findById(entity.getId());
        Entity currentEntity = optionalEntity.orElseThrow(() ->
                new UserNotFoundException("entity with id" + entity.getId() + "not found", HttpStatus.BAD_REQUEST));
        currentEntity.setName(entity.getName());

        return repository.save(currentEntity);
    }
}
