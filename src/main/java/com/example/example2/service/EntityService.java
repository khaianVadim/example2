package com.example.example2.service;

import com.example.example2.domain.Entity;

import java.util.List;

public interface EntityService {

    List<Entity> getAll();

    Entity findById(Long id);

    Entity updateCreate(Entity entity);
}
